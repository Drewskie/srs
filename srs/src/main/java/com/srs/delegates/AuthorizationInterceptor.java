package com.srs.delegates;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.srs.data.User;
import com.srs.data.UserRepository;

/**
 * This class exists for a silly reason. While creating this project, I just didn't have
 * immediate interest in diving into the Spring Security module. It old myself I'd get
 * around to it later, and in the mean time, create a very basic login system with a simple
 * hashed password. This interceptor steps in front of any request made to the server,
 * checks credentials, and if they fail, sends the user back to the login page. 
 * 
 * @author Andrew Davison
 */
@Component
public class AuthorizationInterceptor extends HandlerInterceptorAdapter
{
	private static UserRepository user_repository;

	@Autowired
	public void setUserRepository(UserRepository user_repository)
	{
		AuthorizationInterceptor.user_repository = user_repository;
	}
	
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
	{
		if (request.getCookies() == null)
		{
			try
			{
				response.sendRedirect("/");
			}
			catch (IOException e)
			{	
				e.printStackTrace();
			}
			
			return false;
		}
		
		List<Cookie> cookies = Arrays.asList(request.getCookies());
		
		String username = "";
		String login_hash = "";
		
		for (Cookie cookie : cookies)
		{
			if (cookie.getName().equals("username")) username = cookie.getValue();
			if (cookie.getName().equals("login_hash")) login_hash = cookie.getValue();
		}
		
		if (username.isEmpty() || login_hash.isEmpty()) return false;
		
		User cur = user_repository.findByUsername(username);
		
		if (!cur.checkLoginHash(login_hash))
		{
			try
			{
				response.sendRedirect("/");
			}
			catch (IOException e)
			{	
				e.printStackTrace();
			}
			
			return false;
		}

		return true;
	}
}
