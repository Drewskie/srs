package com.srs.delegates.unauth;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.srs.data.User;
import com.srs.data.UserRepository;

@RestController
public class UserController
{
	@Autowired
	private UserRepository user_repository;

	@RequestMapping("/login")
	public @ResponseBody String login(	@RequestParam(value="username", defaultValue="") String username,
										@RequestParam(value="password", defaultValue="") String password)
	{
		boolean auth = false;
		String message = "";
		
		if (username.isEmpty() || password.isEmpty()) message = "Login Failed; invalid username or password";
		
		User cur = user_repository.findByUsername(username);
		
		if (cur != null && cur.checkPassword(password))
		{
			auth = true;
			message = "Login Successful";
			
		}
		else
		{
			message = "Login Failed; invalid username or password";	
		}

		StringWriter w = new StringWriter();
		try
		{
			JsonGenerator g = new JsonFactory().createGenerator(w);
			
			if (auth)
			{
				g.writeStartObject();
				{
					g.writeBooleanField("auth", auth);
					g.writeStringField("username", cur.getUsername());
					g.writeStringField("login_hash", cur.getLoginhash());
					g.writeStringField("message", message);
				}
				g.writeEndObject();	

				user_repository.save(cur);
			}
			else
			{
				g.writeStartObject();
				{
					g.writeBooleanField("auth", auth);
					g.writeStringField("message", message);
				}
				g.writeEndObject();	
			}

			g.close();
			
			return w.toString();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}

		return "";
	}
	
	@RequestMapping("/create-user")
	public @ResponseBody String createUser(	@RequestParam(value="username", defaultValue="") String username,
											@RequestParam(value="password", defaultValue="") String password,
											@RequestParam(value="reenter_password", defaultValue="") String reenter_password,
											@RequestParam(value="recaptcha", defaultValue="") String recaptcha_resposne,
											HttpServletRequest request)
	{
		String error_string = null;
		
		if (username.isEmpty() || password.isEmpty() || reenter_password.isEmpty()) error_string = "Please fill out all fields";
		else if (!checkRecaptcha(recaptcha_resposne, request)) error_string = "Recaptcha failed";
		else if (username.length() < 4) error_string = "Username must be at least four characters long";
		else if (password.length() < 6) error_string = "Password must be at least six characters long";
		else if (!password.equals(reenter_password)) error_string = "Passwords do not match";
		else if (user_repository.findByUsername(username) != null) error_string = "Username already in use";
		
		if (error_string != null)
		{
			StringWriter w = new StringWriter();
			
			try
			{
				JsonGenerator g = new JsonFactory().createGenerator(w);
				g.writeStartObject();
				{
					g.writeBooleanField("auth", false);
					g.writeStringField("message", error_string);
				}
				g.writeEndObject();	
				
				g.close();
			}
			catch (IOException e)
			{
				e.printStackTrace();
				return "";
			}
			
			return w.toString();
		}
		
		User user = new User(username, User.generateLoginHash(password));
		
		user_repository.insert(user);
		
		StringWriter w = new StringWriter();
		try
		{
			JsonGenerator g = new JsonFactory().createGenerator(w);
			
			g.writeStartObject();
			{
				g.writeBooleanField("auth", true);
				g.writeStringField("username", user.getUsername());
				g.writeStringField("login_hash", user.getLoginhash());
			}
			g.writeEndObject();	

			user_repository.save(user);

			g.close();
			
			return w.toString();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}

		return "";
	}
	
	private boolean checkRecaptcha(String recaptcha_response, HttpServletRequest request)
	{
		try
		{
			URL url = new URL("https://www.google.com/recaptcha/api/siteverify?secret=6LdWaDoUAAAAAKtJtwwC6RYSS6e_TodLXuNJxkvJ&response="+recaptcha_response+"&remoteip="+request.getRemoteAddr());
	        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
	        connection.setRequestMethod("GET");
	        
	        String line, response_text = "";
	        BufferedReader reader = new BufferedReader( new InputStreamReader(connection.getInputStream()));
	        while ((line = reader.readLine()) != null) 
	        {
	        	response_text += line;
	        }

	        ObjectMapper mapper = new ObjectMapper();
	        JsonNode json_response = mapper.readTree(response_text);

	        return json_response.get("success").asBoolean();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return false;
	}
}
