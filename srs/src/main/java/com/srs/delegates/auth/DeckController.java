package com.srs.delegates.auth;

import java.io.IOException;
import java.io.StringWriter;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.srs.data.Card;
import com.srs.data.CardMetaData;
import com.srs.data.CardMetaDataRepository;
import com.srs.data.CardRepository;
import com.srs.data.Deck;
import com.srs.data.DeckRepository;
import com.srs.data.User;
import com.srs.data.UserRepository;

@RestController
@RequestMapping("/deck")
public class DeckController
{
	@Autowired
	private DeckRepository deck_repository;
	
	@Autowired
	private UserRepository user_repository;
	
	@Autowired
	private CardRepository card_repository;
	
	@Autowired
	private CardMetaDataRepository card_meta_data_repository;

	@RequestMapping("all-deck-info")
	public @ResponseBody String getAllDeckInfo(	@CookieValue("username") String username,
												@RequestParam(value="id", defaultValue="") String id)
	{
		if (id.isEmpty()) return "undefined";

		Deck cur_deck = deck_repository.findById(id);
		if (!cur_deck.getUsername().equals(username)) return "undefined";
		
		List<Card> cards = card_repository.findAllByIds((String[]) cur_deck.getCardIds().toArray(new String[0]));
		
		try
		{
			StringWriter w = new StringWriter();
			JsonGenerator g = new JsonFactory().createGenerator(w);
			
			g.writeStartObject();
			{
				g.writeObjectField("name", cur_deck.getName());
				g.writeObjectField("id", cur_deck.getId());
				g.writeObjectField("created_time", cur_deck.getCreationTime());
				
				g.writeArrayFieldStart("cards");
				{
					for (Card card : cards)
					{
						g.writeStartObject();
						{
							g.writeObjectField("front", card.getFront());
							g.writeObjectField("back", card.getBack());
							g.writeObjectField("card_id", card.getId());
							g.writeObjectField("deck_id", card.getDeckId());
							
							CardMetaData meta_data = card_meta_data_repository.findByCardId(card.getId());
							if (meta_data != null)
							{
								g.writeObjectField("repcount", meta_data.getRepcount());
								g.writeObjectField("last_interval", meta_data.getLastIntervalInDays());
							}
						}
						g.writeEndObject();
					}
				}
				g.writeEndArray();
			}
			g.writeEndObject();
			
			g.close();
			
			return w.toString();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}	
		
		return "undefined";
	}
	
	@RequestMapping("/list-decks")
	public @ResponseBody String listDecks(@CookieValue("username") String username)
	{
		User cur_user = user_repository.findByUsername(username);
		
		if (cur_user.getDeckIds().isEmpty())
		{
			return "{}";
		}
		
		List<Deck> users_decks = deck_repository.findAllByIds((String[]) cur_user.getDeckIds().toArray(new String[0]));
		
		try
		{
			StringWriter w = new StringWriter();
			JsonGenerator g = new JsonFactory().createGenerator(w);
			
			g.writeStartArray();
			{
				for (Deck deck : users_decks)
				{
					g.writeStartObject();
					{
						g.writeObjectField("name", deck.getName());
						g.writeObjectField("id", deck.getId());
					}
					g.writeEndObject();
				}
			}
			g.writeEndArray();
			
			g.close();
			
			return w.toString();
		}
		catch (IOException e)
		{
			e.printStackTrace();
			return "{}";
		}
	}
	
	@RequestMapping("/add-deck")
	public @ResponseBody String addDeck(@CookieValue("username") String username, 
										@RequestParam(value="name", defaultValue="") String name)
	{
		if (name.isEmpty()) return "Invalid name";
		
		User cur_user = user_repository.findByUsername(username);
		List<String> users_deck_ids = cur_user.getDeckIds();
		
		if (users_deck_ids.contains(name)) return "A deck with this name already exists";

		Deck deck = new Deck(name, username);
		deck_repository.insert(deck);
		
		cur_user.addDeckId(deck.getId());
		user_repository.save(cur_user);
		
		return "success";
	}
	
	@RequestMapping("/edit-deck")
	public @ResponseBody String editDeck(@CookieValue("username") String username, 
										@RequestParam(value="name", defaultValue="") String name,
										@RequestParam(value="id", defaultValue="") String id)
	{
		if (name.isEmpty()) return "Invalid name";
		if (id.isEmpty()) return "Invalid ID";
		
		User cur_user = user_repository.findByUsername(username);
		List<String> users_deck_ids = cur_user.getDeckIds();
		
		if (users_deck_ids.contains(name)) return "A deck with this name already exists";

		Deck deck = deck_repository.findById(id);
		deck.setName(name);
		deck_repository.save(deck);
		
		return "success";
	}
	
	@RequestMapping("/delete-deck")
	public @ResponseBody boolean deleteDeck(@CookieValue("username") String username, 
			@RequestParam(value="id", defaultValue="") String id)
	{
		if (id.isEmpty()) return false;

		Deck deck = deck_repository.findById(id);
		if (!deck.getUsername().equals(username)) return false;
		
		deck_repository.delete(deck);
		
		/*
		 * TODO Also delete all related cards and repsets
		 */
		
		return true;
	}
}
