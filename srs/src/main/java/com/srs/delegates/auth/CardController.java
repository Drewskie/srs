package com.srs.delegates.auth;

import java.io.IOException;
import java.io.StringWriter;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.srs.data.Card;
import com.srs.data.CardMetaData;
import com.srs.data.CardMetaDataRepository;
import com.srs.data.CardRepository;
import com.srs.data.Deck;
import com.srs.data.DeckRepository;

@RestController
@RequestMapping("/card")
public class CardController
{
	@Autowired
	private CardRepository card_repository;
	
	@Autowired
	private DeckRepository deck_repository;
	
	@Autowired
	private CardMetaDataRepository card_meta_data_repository;
	
	@RequestMapping("/list-cards")
	public @ResponseBody String listCards(	@CookieValue("username") String username,
											@RequestParam(value="deck_id", defaultValue="") String deck_id)
	{
		Deck cur_deck = deck_repository.findById(deck_id);
		if (!cur_deck.getUsername().equals(username)) return "{}";
		
		if (cur_deck.getCardIds().isEmpty())
		{
			return "{}";
		}
		
		List<Card> cards = card_repository.findAllByIds((String[]) cur_deck.getCardIds().toArray(new String[0]));
		
		try
		{
			StringWriter w = new StringWriter();
			JsonGenerator g = new JsonFactory().createGenerator(w);
			
			g.writeStartArray();
			{
				for (Card card : cards)
				{
					g.writeStartObject();
					{
						g.writeObjectField("front", card.getFront());
						g.writeObjectField("back", card.getBack());
						g.writeObjectField("id", card.getId());
						g.writeObjectField("deck_id", card.getDeckId());
					}
					g.writeEndObject();
				}
			}
			g.writeEndArray();
			
			g.close();
			
			return w.toString();
		}
		catch (IOException e)
		{
			e.printStackTrace();
			return "{}";
		}
	}
	
	@RequestMapping("/get-card-info")
	public @ResponseBody String getCardInfo(@CookieValue("username") String username, 
			@RequestParam(value="card_id", defaultValue="") String card_id)
	{
		Card card = card_repository.findById(card_id);
		if (card == null) return "{}";
		
		if (!card.getUsername().equals(username)) return "{}";
		
		try
		{
			StringWriter w = new StringWriter();
			JsonGenerator g = new JsonFactory().createGenerator(w);

			g.writeStartObject();
			{
				g.writeObjectField("front", card.getFront());
				g.writeObjectField("back", card.getBack());
				g.writeObjectField("id", card.getId());
			}
			g.writeEndObject();
			
			g.close();
			
			return w.toString();
		}
		catch (IOException e)
		{
			e.printStackTrace();
			return "{}";
		}
	}
	
	@RequestMapping("/edit-card")
	public @ResponseBody String editCard(@CookieValue("username") String username, 
										@RequestParam(value="card_id", defaultValue="") String card_id,
										@RequestParam(value="front", defaultValue="") String front,
										@RequestParam(value="back", defaultValue="") String back)
	{
		if (card_id.isEmpty()) return "Invalid card";
		if (front.isEmpty()) return "Card must have front";
		if (back.isEmpty()) return "Card must have back";
		
		Card cur_card = card_repository.findById(card_id);
		if (!cur_card.getUsername().equals(username)) return "Invalid card";
		
		Card duplicate = card_repository.findByDeckIdAndFrontAndIdNot(cur_card.getDeckId(), front, card_id);
		if (duplicate != null) return "Card has duplicate front.";

		cur_card.setFront(front);
		cur_card.setBack(back);
		
		card_repository.save(cur_card);

		return "success";
	}
	
	@RequestMapping("/add-card")
	public @ResponseBody String addCard(@CookieValue("username") String username, 
										@RequestParam(value="deck_id", defaultValue="") String deck_id,
										@RequestParam(value="front", defaultValue="") String front,
										@RequestParam(value="back", defaultValue="") String back)
	{
		if (deck_id.isEmpty()) return "Invalid deck";
		if (front.isEmpty()) return "Card must have front";
		if (back.isEmpty()) return "Card must have back";
		
		Card duplicate = card_repository.findByDeckIdAndFront(deck_id, front);
		if (duplicate != null) return "Card has duplicate front.";
		
		Deck cur_deck = deck_repository.findById(deck_id);
		if (!cur_deck.getUsername().equals(username)) return "Invalid deck";

		Card card = new Card(front, back, username, deck_id);
		card_repository.insert(card);
		
		CardMetaData meta_data = new CardMetaData(username, card.getId(), deck_id);
		card_meta_data_repository.insert(meta_data);
		
		cur_deck.addCardId(card.getId());
		deck_repository.save(cur_deck);
		
		RepsController.addCardToRepSet(RepsController.getTodaysDateRounded(), card.getId(), cur_deck.getId(), username, false);
		
		return "success";
	}
	
	@RequestMapping("/delete-card")
	public @ResponseBody boolean deleteCard(@CookieValue("username") String username, 
			@RequestParam(value="deck_id", defaultValue="") String deck_id,
			@RequestParam(value="card_id", defaultValue="") String card_id)
	{
		if (deck_id.isEmpty()) return false;
		if (card_id.isEmpty()) return false;

		Card card = card_repository.findById(card_id);
		if (!card.getUsername().equals(username)) return false;
		
		card_repository.delete(card);
		
		CardMetaData meta_data = card_meta_data_repository.findByCardId(card_id);
		card_meta_data_repository.delete(meta_data);
		
		Deck deck = deck_repository.findById(deck_id);
		deck.removeCardId(card_id);
		
		deck_repository.save(deck);
		
		RepsController.removeCardFromAnyRepSet(deck_id, card_id);

		return true;
	}
}
