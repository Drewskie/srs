package com.srs.delegates.auth;

import java.io.IOException;
import java.io.StringWriter;
import java.net.URLDecoder;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.srs.data.Card;
import com.srs.data.CardMetaData;
import com.srs.data.CardMetaDataRepository;
import com.srs.data.CardRepository;
import com.srs.data.DeckRepository;
import com.srs.data.RepSet;
import com.srs.data.RepSetRepository;

/**
 * This controller is really the meat of the entire system. In this class, we handle the delivery
 * and completion of reps, as well as scheduling reps according to the memory algorithm. Additional
 * documentation is attached to the relevant methods.
 * 
 * @author Andrew Davison
 */
@RestController
@RequestMapping("/reps")
public class RepsController
{
	@Autowired
	private CardRepository card_repository;

	@Autowired
	private CardMetaDataRepository card_meta_data_repository;
	
	@Autowired
	private DeckRepository deck_repository;
	
	private static RepSetRepository reps_repository;

	@Autowired
	public void setRepSetRepository(RepSetRepository reps_repository)
	{
		RepsController.reps_repository = reps_repository;
	}
	
	/**
	 * Records a repetition, moves the rep to a new repset, and calculates/applies its next
	 * due date using {@link #applyMemoryAlgorithm(Card, String, String, int) applyMemoryAlgorithm}.
	 */
	@RequestMapping("record-rep")
	public @ResponseBody String recordRep(	@CookieValue("username") String username,
											@RequestParam(value="deck_id", defaultValue="") String deck_id,
											@RequestParam(value="current_rep_set_id", defaultValue="") String current_rep_set_id,
											@RequestParam(value="card_id", defaultValue="") String card_id,
											@RequestParam(value="rating", defaultValue="-1") int rating) throws IOException
	{
		if (deck_id.isEmpty()) return "{}";
		if (current_rep_set_id.isEmpty()) return "{}";
		if (card_id.isEmpty()) return "{}";
		if (rating == -1) return "{}";

		RepSet current_rep_set = reps_repository.findById(current_rep_set_id);
		if (current_rep_set == null) return "{}";
		
		current_rep_set.removeRep(card_id);
		reps_repository.save(current_rep_set);
		
		if (current_rep_set.isEmpty()) reps_repository.delete(current_rep_set);
		
		Card card = card_repository.findById(card_id);
		CardMetaData meta_data = card_meta_data_repository.findByCardId(card_id);
		
		int next_interval = applyMemoryAlgorithm(card, meta_data, deck_id, username, rating);
		
		StringWriter w = new StringWriter();
		JsonGenerator g = new JsonFactory().createGenerator(w);
		
		String decoded_card_front = URLDecoder.decode(card.getFront(), "UTF-8");
		String card_summary = decoded_card_front.length() > 20 ? decoded_card_front.substring(0,  20) : decoded_card_front;
		
		g.writeStartObject();
		{
			g.writeBooleanField("success", true);
			g.writeStringField("card_name", card_summary);
			g.writeNumberField("next_interval", next_interval);
		}
		g.writeEndObject();
		
		g.close();
		
		return w.toString();
	}

	 /**
	  * Returns a repset for the user to work on. This function is the victim of a little friction with 
	  * the frontend. It does a little extra work to make sure it doesn't return the same rep set that's
	  * currently being worked on by the user.
	  */
	@RequestMapping("get-due-rep-set")
	public @ResponseBody String getDueRepSet(	@CookieValue("username") String username,
												@RequestParam(value="deck_id", defaultValue="") String deck_id,
												@RequestParam(value="current_rep_set_id", defaultValue="") String current_rep_set_id,
												@RequestParam(value="completed_rep_sets", defaultValue="") String completed_rep_set_ids_string)
	{
		if (deck_id.isEmpty()) return "undefined";
		
		List<RepSet> due_rep_sets = reps_repository.findRepsetsForDeckDueOnDate(deck_id, getTodaysDateRounded());
		if (due_rep_sets.isEmpty()) return "{}";

		if (due_rep_sets.size() == 1)
		{
			//Do nothing until current_rep_set_id is empty, i.e. the front-end is finished with it.
			if (current_rep_set_id.isEmpty()) return RepSet.repSetToJSON(due_rep_sets.get(0), card_repository); 
		}
		//Get a different repset
		else
		{
			Collections.shuffle(due_rep_sets);
			
			for (RepSet rep_set : due_rep_sets)
			{
				if (rep_set.getId().equals(current_rep_set_id)) continue;
				if (rep_set.getCardIds().isEmpty()) continue;

				return RepSet.repSetToJSON(rep_set, card_repository); 
			}	
		}

		return "{}";
	}

	/**
	 * A debug function that returns all of the user's repsets. 
	 */
	@RequestMapping("all-rep-sets")
	public @ResponseBody String getAllRepSets(	@CookieValue("username") String username,
												@RequestParam(value="deck_id", defaultValue="") String deck_id)
	{
		if (deck_id.isEmpty()) return "undefined";
		
		List<RepSet> all_rep_sets = reps_repository.findAllByDeckId(deck_id);
		if (all_rep_sets == null || all_rep_sets.isEmpty()) return "undefined";
		
		try
		{
			StringWriter w = new StringWriter();
			JsonGenerator g = new JsonFactory().createGenerator(w);
			
			g.writeStartArray();
			{
				for (RepSet rep_set : all_rep_sets)
				{
					RepSet.repSetToJSON(g, rep_set, card_repository);
				}
			}
			g.writeEndArray();
			
			g.close();
			
			return w.toString();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}	
		
		return "undefined";
	}
	
	/**
	 * A debug function that puts all cards into new repsets due today.
	 */
	@RequestMapping("reset-all-repsets")
	public @ResponseBody void resetAllRepsets(		@CookieValue("username") String username,
													@RequestParam(value="deck_id", defaultValue="") String deck_id)
	{
		if (deck_id.isEmpty()) return;
		
		List<RepSet> all_rep_sets = reps_repository.findAllByDeckId(deck_id);
		if (all_rep_sets == null) return;

		reps_repository.delete(all_rep_sets);

		List<String> all_card_ids = deck_repository.findById(deck_id).getCardIds();

		for (String card_id : all_card_ids)
		{
			addCardToRepSet(getTodaysDateRounded(), card_id, deck_id, username, false);	
		}
	}
	
	/**
	 * A debug function that resets all cards to their virgin state.
	 */
	@RequestMapping("reset-all-card-ratings")
	public @ResponseBody void resetAllCardRatings(	@CookieValue("username") String username,
													@RequestParam(value="deck_id", defaultValue="") String deck_id)
	{
		if (deck_id.isEmpty()) return;
		
		List<RepSet> all_rep_sets = reps_repository.findAllByDeckId(deck_id);
		if (all_rep_sets == null || all_rep_sets.isEmpty()) return;

		reps_repository.delete(all_rep_sets);

		List<String> all_card_ids = deck_repository.findById(deck_id).getCardIds();

		for (String card_id : all_card_ids)
		{
			CardMetaData card_meta_data = card_meta_data_repository.findById(card_id);
			card_meta_data.resetToDefaultDifficulty();
			card_meta_data_repository.save(card_meta_data);
		}
		
		resetAllRepsets(username, deck_id);
	}
	
	public static void removeCardFromAnyRepSet(String deck_id, String card_id)
	{
		List<RepSet> all_decks_repsets = reps_repository.findAllByDeckId(deck_id);
		
		for (RepSet rep_set : all_decks_repsets)
		{
			if (rep_set.getCardIds().contains(card_id))
			{
				rep_set.removeRep(card_id);
				reps_repository.save(rep_set);
			}
		}
	}
	
	/**
	 * This function attempts to determine the best amount of time that should pass before the
	 * user sees the piece of information on a card again. People write dissertations on this problem,
	 * and luckily, the result of their work is widely available. The algorithm at the root of
	 * this is used in SuperMemo 2.0, which despite being one of the oldest SRS programs around,
	 * and despite being the basis for at least a few prominent SRS programs, is little used itself,
	 * as far as I know.
	 * 
	 * I've used Anki, a more pure implementation of SM2, and Surusu, a loose one, and I vastly prefer 
	 * Surusu's loose algorithm. SM2 in its purest form heavily prioritizes retention at the expense 
	 * of sanity, and punishes any mistake or fluke failure very heavily.
	 * 
	 * Sanity is the most important part of any memory-based project. If it takes an immense
	 * expenditure of will and focus to make yourself do your flash cards, you're not going to do
	 * them, or at least, not without severe pressure or some threat looming over your head. SM2's
	 * algorithm attacks sanity by allowing cards to stay just on the edge of your ability to remember
	 * them, finding a "Hard" rating to be acceptable, and never giving you the opportunity to grasp
	 * it a little better. Once a deck hits 1000+ cards, you wind up with 10-20% of your cards showing
	 * up far too frequently, and yet you still can't quite remember them well, straining your mind for
	 * no good reason.
	 * 
	 * In SM2's algorithm, if for some reason you forget a card that you had learned pretty well,
	 * hitting the "missed" button causes that card to essentially drop down to being seen every day 
	 * again, and it takes weeks for it to go back to its old long interval. A card you used to see every 
	 * 4 months becomes a card you see every week, which in many cases feels like a frustrating waste of time.
	 * 
	 * So, this algorithm takes some steps for comfort. "Hard" ratings *reduce* their interval just
	 * slightly, so you see them sooner and can get a more firm grip on them. Cards that are missed
	 * are seen again soon, but the algorithm remembers the old rating, and if you hit "Easy"
	 * again, it rapidly increases the interval by weeks at a time. There's also some tweaks for
	 * interval fuzzing (adding 100 cards and then seeing 80 of them on a day when you would normally
	 * have only 30 cards is a real drag), the difficulty increase values themselves (found after playing
	 * with a spreadsheet for a while), and card suspension to make sure a forgotten card is properly 
	 * remembered before being rescheduled.
	 */
	private int applyMemoryAlgorithm(Card card, CardMetaData card_meta_data, String deck_id, String username, int rating)
	{
		if (card_meta_data == null) return -1;
		
		//Intervals are the number of days between two repetitions on the same card.
		int last_interval = card_meta_data.getLastIntervalInDays();
		int next_interval = card_meta_data.getLastIntervalInDays();
		
		//"Easiness Factor" is a value used to increase intervals based on the user's cumulative experience with the card.
		double easiness_multiplier = card_meta_data.getIntervalEasinessMultiplier();
		
		//This is a bonus applied to the next interval to soften the blow of forgetting very old cards.
		int days_adjustment = card_meta_data.getNextIntervalAdjustmentInDays();
		
		/*
		 * This tracks if a card is currently in "relearning limbo," freshly missed today and being passed 
		 * through the session until the user hits "Good" or "Easy."
		 */
		boolean was_suspended_on_arrival = card_meta_data.isCurrentlySuspendedWhileRelearning();
		
		Date today = getTodaysDateRounded();
		
		//"Blank", i.e. a complete loss of memory of the card.
		if (rating == 0)
		{
			/*
			 * If the card is not currently suspended, do a single round of calculations, then suspend it. 
			 */
			if (!card_meta_data.isCurrentlySuspendedWhileRelearning())
			{
				days_adjustment = (int) (last_interval * .33);
				easiness_multiplier = 1;
				next_interval = 1;
				
				card_meta_data.setIsCurrentlySuspendedWhileRelearning(true);
			}
		}
		//"Miss", i.e. any failure to recall the card; not as severe a failure as "Blank"
		else if (rating == 1)
		{
			/*
			 * If the card is not currently suspended, do a single round of calculations, then suspend it. 
			 */
			if (!card_meta_data.isCurrentlySuspendedWhileRelearning())
			{
				days_adjustment = (int) (last_interval * .75);
				easiness_multiplier = 1;
				next_interval = 1;
				
				card_meta_data.setIsCurrentlySuspendedWhileRelearning(true);
			}
		}
		//"Hard", i.e. the card was difficult to recall, but the user was successful.
		else if (rating == 2)
		{
			/*
			 * Hard ratings don't suspend cards, but they don't lift suspensions.
			 * If we arrive here, it's because the user is struggling to recall a
			 * card after multiple tries, and needs to keep running through it until
			 * they hit the "Good" button. So we only take real action here if the card
			 * is NOT suspended.
			 */
			if (!card_meta_data.isCurrentlySuspendedWhileRelearning())
			{
				days_adjustment = 0;
				easiness_multiplier = 1;
				next_interval = (int) (last_interval * .8);
				if (next_interval < 1) next_interval = 1;
			}
		}
		//"Good", i.e. the user remembered the card comfortably.
		else if (rating == 3)
		{
			//Remove any suspensions
			card_meta_data.setIsCurrentlySuspendedWhileRelearning(false);
			
			if (easiness_multiplier < 1.4) easiness_multiplier = 1.4;
			
			next_interval = (int) (last_interval * easiness_multiplier);
			if (next_interval < 3) next_interval = 3;
			
			days_adjustment = 0;
			easiness_multiplier += .1;
		}
		//"Easy", i.e. the user remembered the card without any effort. The user was likely shown this too soon, so I push cards away pretty aggressively here.
		else if (rating == 4)
		{
			//Remove any suspensions
			card_meta_data.setIsCurrentlySuspendedWhileRelearning(false);

			if (easiness_multiplier < 1.7) easiness_multiplier = 1.7;
			next_interval = (int) (last_interval * easiness_multiplier);
			if (next_interval < 5) next_interval = 5;
			
			/*
			 * Tack on any days adjustment, for brain farts.
			 */
			if (days_adjustment > 14)
			{
				next_interval += 14;
				days_adjustment -= 14;
			}
			else
			{
				next_interval += days_adjustment;
				days_adjustment = 0;
			}
			
			easiness_multiplier += .3;
		}
		
		/*
		 * Fuzz longer intervals by plus or minus a day. This prevents issues where unusually
		 * large additions -- say, 100 cards on a single day to a 1000 card deck -- cause bubbles
		 * where a deck that produces 20 due reps per day suddenly produces 80 out of nowhere, as
		 * that huge batch slowly gets worked on. This will still happen in the short-term, but once
		 * an interval is over 7 days, we spread those cards out a little, hoping that the +/- 1 won't
		 * have a huge impact on recall rate.
		 */
		if (rating >= 3 && next_interval > 7)
		{
			Random random = new Random(System.currentTimeMillis());
			int day_spreader = random.nextInt(3) - 1;
			
			next_interval += day_spreader;
		}
		
		//Max easiness multiplier of 3. Intervals get crazy without this limit.
		if (easiness_multiplier > 3.0) easiness_multiplier = 3.0;

		/*
		 * Do not record any data if a card is stuck in suspension. Just add it to a repset
		 * so it gets sent back to the user during their current session.
		 */
		if (was_suspended_on_arrival && card_meta_data.isCurrentlySuspendedWhileRelearning())
		{
			addCardToRepSet(today, card_meta_data.getId(), deck_id, username, true);
			return 0;
		}
		else
		{
			card_meta_data.setIntervalEasinessMultiplier(easiness_multiplier);
			card_meta_data.setNextIntervalAdjustmentInDays(days_adjustment);
			card_meta_data.setLastIntervalInDays(next_interval);
			
			if (card_meta_data.isCurrentlySuspendedWhileRelearning())
			{
				addCardToRepSet(today, card_meta_data.getId(), deck_id, username, true);
				
				System.out.println(card.getFront().length() > 40? card.getFront().substring(0,  40) : card.getFront());
				System.out.println(card_meta_data.getId());
				System.out.println("Last interval: " + card_meta_data.getLastIntervalInDays());
				System.out.println("Easiness Factor: " + card_meta_data.getIntervalEasinessMultiplier());
				System.out.println("Reported difficulty: " + rating);
				System.out.println("Earned (next) interval: " + next_interval);
				System.out.println("Total reps: " + card_meta_data.getRepcount());
				System.out.println("Card repeated");
				System.out.println("*****************************");
			}
			else
			{
				Date next_rep_date = new Date();
				
				Calendar c = Calendar.getInstance();
				c.setTime(next_rep_date);
				c.add(Calendar.DATE, next_interval);
				
				next_rep_date = DateUtils.round(c.getTime(), Calendar.DAY_OF_MONTH);

				System.out.println(card.getFront().length() > 20? card.getFront().substring(0,  20) : card.getFront());
				System.out.println(card_meta_data.getId());
				System.out.println("Last interval: " + card_meta_data.getLastIntervalInDays());
				System.out.println("Easiness Factor: " + card_meta_data.getIntervalEasinessMultiplier());
				System.out.println("Reported difficulty: " + rating);
				System.out.println("Earned (next) interval: " + next_interval);
				System.out.println("Total reps: " + card_meta_data.getRepcount());
				System.out.println("Due date: " + next_rep_date);
				System.out.println("*****************************");
				
				addCardToRepSet(next_rep_date, card_meta_data.getId(), deck_id, username, false);
			}
			
			card_meta_data_repository.save(card_meta_data);
		}
		
		return next_interval;
	}

	/**
	 * Schedule a card to be due on a parameterized due date. This will add the card to an existing repset if one exists,
	 * however a "force_new_repset" boolean parameter allows the developer to force this behavior. At present, that's used
	 * for rescheduling cards for same-day use. Putting them in their own repset allows for easy shuffling.
	 */
	public static void addCardToRepSet(Date due_date, String card_id, String deck_id, String username, boolean force_new_repset)
	{
		RepSet open_repset = reps_repository.findOpenRepsetForDate(deck_id, due_date);
		
		removeCardFromAnyRepSet(deck_id, card_id);
		
		if (open_repset == null || force_new_repset)
		{
			open_repset = new RepSet(due_date, deck_id, username);
			open_repset.addRep(card_id);
			reps_repository.insert(open_repset);
		}
		else
		{
			if (!open_repset.getUsername().equals(username)) return;
			
			open_repset.addRep(card_id);
			reps_repository.save(open_repset);
		}
	}
	
	public static Date getTodaysDateRounded()
	{
		Date today = DateUtils.round(new Date(), Calendar.DAY_OF_MONTH);
		
		Calendar c = Calendar.getInstance();
		c.setTime(today);
		
		today = DateUtils.round(c.getTime(), Calendar.DAY_OF_MONTH);
		return today;
	}
}