package com.srs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class SRS 
{
	public static void main(String[] args)
	{
		/*
		 * Keeping the tutorial bean lister in place while I learn Spring better (it really helped once!)
		 */
		ApplicationContext context = SpringApplication.run(SRS.class, args);
		
		System.out.println("The tutorial says we're inspecting beans now.");
		
		ArrayList<String> bean_names = new ArrayList<>(Arrays.asList(context.getBeanDefinitionNames()));
		Collections.sort(bean_names);
		
		for (String bean_name : bean_names)
		{
			System.out.println(bean_name);
		}
	}
}
