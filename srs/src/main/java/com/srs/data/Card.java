package com.srs.data;

import java.util.ArrayList;

/**
 * A card represents one unit of information learned by the user, stored in decks. This contains
 * all the information necessary to assign the card a new interval, using the algorithm in RepsController.java
 * 
 * @author Andrew Davison
 *
 */
public class Card
{
	private String id;
	private String username;
	private String deckId;
	
	private String front;
	private String back;
	

	public Card(String front, String back, String username, String deckId)
	{
		this.front = front;
		this.back = back;
		this.username = username;
		this.deckId = deckId;
	}
	
	public String getId() { return id; }
	public String getFront() { return front; }
	public String getBack() { return back; }
	public String getUsername() { return username; }
	public String getDeckId() { return deckId; }
	
	public void setFront(String new_front) { front = new_front; }
	public void setBack(String new_back) { back = new_back; }
	public void setDeckId(String deck_id) { deckId = deck_id; }
}
