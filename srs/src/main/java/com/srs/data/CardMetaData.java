package com.srs.data;

import java.util.ArrayList;

/**
 * CardMetaData corresponds to a Card, and contains all the information necessary to assign the card a new interval, 
 * using the algorithm in RepsController.java. It also contains some historical data on the card's performance.
 * 
 * 
 * @author Andrew Davison
 *
 */
public class CardMetaData
{
	private String id;
	private String username;
	private String cardId;
	private String deckId;
	private int repcount;
	
	/*
	 * Used in the algorithm in RepsController to record how difficult a card *has been* as opposed to 
	 * how difficult it was on the last rep.
	 */
	private double interval_easiness_multiplier = 2.5;
	
	/*
	 * The length of the interval (the time between reps) assigned after the last repetition.
	 */
	private int last_interval_in_days = 1;
	
	/*
	 * A bonus applied to the next interval to soften the blow of forgetting very old cards.
	 */
	private int next_interval_adjustment_in_days = 0;
	
	/*
	 * The last ten intervals; used to measure card health.
	 */
	private ArrayList<Integer> last_ten_intervals_in_days = new ArrayList<>(10);
	
	/*
	 * A flag that forces the user to re-rep this card until they hit "Good" or "Easy"
	 * after reporting a "Blank" or a "Miss."
	 */
	private boolean currently_suspended_while_relearning = false;
	
	public CardMetaData(String username, String cardId, String deckId)
	{
		this.username = username;
		this.cardId = cardId;
		this.deckId = deckId;
	}
	
	public String getId() { return id; }
	public String getUsername() { return username; }
	public String getCardId() { return cardId; }
	public String getDeckId() { return deckId; }
	public int getRepcount() { return repcount; }
	public double getIntervalEasinessMultiplier() { return interval_easiness_multiplier; }
	public boolean isCurrentlySuspendedWhileRelearning() { return currently_suspended_while_relearning; }
	public int getLastIntervalInDays() { return last_interval_in_days; }
	public int getNextIntervalAdjustmentInDays() { return next_interval_adjustment_in_days; }
	public ArrayList<Integer> getIntervalHistory() { return last_ten_intervals_in_days; }
	
	public void setCardId(String card_id) { cardId = card_id; }
	public void setDeckId(String deck_id) { deckId = deck_id; }
	private void incrementRepcount() { repcount++; }
	public void setRepcount(int repcount) { this.repcount = repcount; }
	public void setIntervalEasinessMultiplier(double new_multiplier) { interval_easiness_multiplier = new_multiplier; }
	public void setIsCurrentlySuspendedWhileRelearning(boolean is_suspended) { currently_suspended_while_relearning = is_suspended; }
	public void setNextIntervalAdjustmentInDays(int new_adjustment) { next_interval_adjustment_in_days = new_adjustment; }
	public void setIntervalHistory(ArrayList<Integer> history) { last_ten_intervals_in_days = history; }
	
	public void setLastIntervalInDays(int new_interval)
	{ 
		incrementRepcount();
		
		last_ten_intervals_in_days.add(last_interval_in_days);
		if (last_ten_intervals_in_days.size() > 10) last_ten_intervals_in_days.remove(0);
		
		last_interval_in_days = new_interval; 
	}
	
	/**
	 * Determines whether or not a card is healthy by determining if the last interval
	 * is within 30% of the average of the last 10 intervals. Basically, a card is
	 * unhealthy if the user's interval is not growing over time.
	 */
	public boolean isCardHealthy()
	{
		if (last_ten_intervals_in_days.size() < 5) return true;
		
		int sum = 0;
		for (int interval : last_ten_intervals_in_days) sum += interval;
		double average = sum / last_ten_intervals_in_days.size();

		if (last_interval_in_days < average / 1.3 && last_interval_in_days > average * 1.3) return true;
		
		return false;
	}

	public void resetToDefaultDifficulty()
	{
		setIntervalEasinessMultiplier(2.5);
		setLastIntervalInDays(1);
		setIsCurrentlySuspendedWhileRelearning(false);
		setNextIntervalAdjustmentInDays(0);
		last_ten_intervals_in_days = new ArrayList<>();
		repcount = 0;
	}
}
