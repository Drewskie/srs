package com.srs.data;

import java.util.List;

import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

@Configurable
public interface DeckRepository extends MongoRepository<Deck, String> 
{
	public List<Deck> findAll();
	
	public Deck findById(String id);
	public List<Deck> findAllByUsername(String username);
	
	@Query(value="{ '_id' : { $in: ?0 } }")
	public List<Deck> findAllByIds(String[] ids);
}
