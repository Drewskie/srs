package com.srs.data;

import java.util.List;

import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

@Configurable
public interface CardMetaDataRepository extends MongoRepository<CardMetaData, String> 
{
	public List<CardMetaData> findAll();
	
	public CardMetaData findById(String id);
	public List<CardMetaData> findAllByUsername(String username);

	public CardMetaData findByCardId(String card_id);
}
