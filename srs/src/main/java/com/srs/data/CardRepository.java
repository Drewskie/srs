package com.srs.data;

import java.util.List;

import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

@Configurable
public interface CardRepository extends MongoRepository<Card, String> 
{
	public List<Card> findAll();
	
	public Card findById(String id);
	public List<Card> findAllByUsername(String username);
	
	@Query(value="{ '_id' : { $in: ?0 } }")
	public List<Card> findAllByIds(String[] ids);

	//Used to inspect all cards in a deck for duplicate fronts when creating a new card.
	public Card findByDeckIdAndFront(String deckId, String front);
	
	//Used to inspect all cards in a deck for duplicate fronts when editing an existing card.
	public Card findByDeckIdAndFrontAndIdNot(String deckId, String front, String id);
}
