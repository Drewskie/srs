package com.srs.data;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents a deck of cards in the database. A deck contains lists of its cards and the repsets containing
 * those cards, as well as a connection back to the user the deck belongs to.
 * 
 * @author Andrew Davison
 *
 */
public class Deck
{
	private String id;
	
	private String name;
	private Long creation_time;
	private List<String> card_ids;
	private List<String> rep_set_ids;
	
	private String username;
	
	public Deck(String name, String username)
	{
		this.name = name;
		this.creation_time = System.currentTimeMillis();
		this.card_ids = new ArrayList<>();
		this.rep_set_ids = new ArrayList<>();
		this.username = username;
	}
	
	public String getName() { return name; }
	public String getId() { return id; }
	public Long getCreationTime() { return creation_time; }
	public List<String> getCardIds() { return card_ids; }
	public List<String> getRepSetIds() { return rep_set_ids; }
	public String getUsername() { return username; }
	
	public void setName(String name) { this.name = name; }
	public void addCardId(String card_id) { card_ids.add(card_id); }
	public void removeCardId(String card_id) { card_ids.remove(card_id); }
	
	@Override
	public String toString() { return getName(); }
}
