package com.srs.data;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

@Configurable
public interface RepSetRepository extends MongoRepository<RepSet, String> 
{
	public List<RepSet> findAll();
	public List<RepSet> findAllByDeckId(String deck_id);
	public RepSet findById(String id);
	
	@Query(value="{ 'deckId' : ?0, 'due_date': { $lte: ?1 } }")
	public List<RepSet> findRepsetsForDeckDueOnDate(String deck_id, Date date);
	
	@Query(value="{ 'deckId' : ?0, 'full' : false, 'due_date': ?1 }")
	public RepSet findOpenRepsetForDate(String deck_id, Date date);
}
