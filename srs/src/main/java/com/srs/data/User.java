package com.srs.data;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

/**
 * Represents one user in the database. Users have passwords stored as hashes, and a list of decks.
 * 
 * @author Andrew Davison
 *
 */
public class User
{
	private String id;
	
	private String username;
	private String login_hash;
	
	private List<String> deck_ids;
	
	public User(String username, String login_hash)
	{
		this.username = username;
		this.login_hash = login_hash;
		
		deck_ids = new ArrayList<String>();
	}
	
	public String getId() { return id; }
	public String getUsername() { return username; }
	public String getLoginhash() { return login_hash; }
	public List<String> getDeckIds() { return deck_ids; }
	
	public void addDeckId(String deck_id) { deck_ids.add(deck_id); }
	
	public boolean checkPassword(String password) 
	{ 
		if (password == null) return false;
		
		return generateLoginHash(password).equals(login_hash); 
	} 
	
	public static String generateLoginHash(String plaintext_password)
	{
		MessageDigest m;
		try
		{
			/*
			 * Lifted from https://stackoverflow.com/questions/415953/how-can-i-generate-an-md5-hash
			 */
			m = MessageDigest.getInstance("MD5");
			
			m.reset();
			m.update(plaintext_password.getBytes());
			byte[] digest = m.digest();
			BigInteger bigInt = new BigInteger(1,digest);
			String hashtext = bigInt.toString(16);
			
			while(hashtext.length() < 32 )
			{
			  hashtext = "0"+hashtext;
			}

			return hashtext;
		}
		catch (NoSuchAlgorithmException e)
		{
			//Should never reach this since algorithm is hard-coded above.
			e.printStackTrace();
			return null;
		}
	}

	public boolean checkLoginHash(String hash)
	{
		if (hash == null) return false;
		
		return login_hash.equals(this.login_hash); 
	}
}
