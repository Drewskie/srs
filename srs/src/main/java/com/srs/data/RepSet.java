package com.srs.data;

import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;

/**
 * Represents a set of cards to be delivered to the user when they do flash cards. More documentation on
 * why this data structure exists can be found in reps.html.
 * 
 * @author Andrew Davison
 *
 */
public class RepSet
{
	private final int REPS_PER_REPSET = 5;
	private String id;
	
	private Date due_date;
	private boolean full;
	private List<String> card_ids;
	private String deckId;
	private String username;
	
	public RepSet(Date due_date, String deckId, String username)
	{
		this.due_date = due_date;
		this.deckId = deckId;
		this.username = username;
		this.full = false;
		
		card_ids = new ArrayList<String>();
	}
	
	public void addRep(String card_id)
	{
		card_ids.add(card_id);
		
		setFullStatus();
	}
	
	public void removeRep(String card_id)
	{
		card_ids.remove(card_id);
		
		setFullStatus();
	}
	
	private void setFullStatus()
	{
		if (card_ids.size() >= REPS_PER_REPSET) full = true;
		else full = false;
	}
	
	public Date getDueDate() { return due_date; }
	public boolean isFull() { return full; }
	public boolean isEmpty() { return card_ids.size() == 0; }
	public List<String> getCardIds() { return card_ids; }
	public String getDeckId() { return deckId; }
	public String getUsername() { return username; }
	public String getId() { return id; }
	
	public static void repSetToJSON(JsonGenerator g, RepSet rep_set, CardRepository card_repository)
	{
		try
		{
			g.writeStartObject();
			{
				g.writeObjectField("due_date", rep_set.getDueDate().toGMTString());
				g.writeObjectField("id", rep_set.getId());
				
				List<Card> cards = card_repository.findAllByIds((String[]) rep_set.getCardIds().toArray(new String[0]));
				
				g.writeArrayFieldStart("cards");
				{
					for (Card card : cards)
					{
						g.writeStartObject();
						{
							g.writeObjectField("front", card.getFront());
							g.writeObjectField("back", card.getBack());
							g.writeObjectField("id", card.getId());
						}
						g.writeEndObject();	
					}
				}
				g.writeEndArray();
			}
			g.writeEndObject();
		}
		catch (IOException ioe)
		{
			ioe.printStackTrace();
		}
	}
	
	public static String repSetToJSON(RepSet rep_set, CardRepository card_repository)
	{
		try
		{
			StringWriter w = new StringWriter();
			JsonGenerator g = new JsonFactory().createGenerator(w);

			RepSet.repSetToJSON(g, rep_set, card_repository);

			g.close();
			
			return w.toString();
		}
		catch (IOException e)
		{
			e.printStackTrace();
			return "{}";
		}	
	}
}
