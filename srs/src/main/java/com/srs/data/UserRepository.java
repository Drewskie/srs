package com.srs.data;

import java.util.List;

import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.data.mongodb.repository.MongoRepository;

@Configurable
public interface UserRepository extends MongoRepository<User, String> 
{
	public List<User> findAll();
	public User findByUsername(String username);
}
