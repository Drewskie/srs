package com.srs.configs;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.srs.delegates.AuthorizationInterceptor;

@Configuration
public class WebConfig extends WebMvcConfigurerAdapter
{
	@Override
	public void addInterceptors(InterceptorRegistry registry)
	{
		registry.addInterceptor(new AuthorizationInterceptor()).addPathPatterns("/deck/*", "/card/*", "/reps/*");
	}
}
