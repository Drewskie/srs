showCurrentUser();

function loginByEnter(event)
{
	if (event.keyCode == 13) login(); //enter key
}

function login()
{
	$('#sign-in').modal('close');
	var username = $("#username").val();
	var password = $("#password").val();
	
	$.ajax({url:"login", data:{'username':username,'password':password}, success: onLoginResult});
}

function onLoginResult(result_json)
{
	var result = JSON.parse(result_json);

	if (result.auth)
	{
		var expires_in_one_year = new Date(new Date().getTime() + 1000 * 60 * 60 * 24 * 365);
		
		Cookies.set('username', result.username, {expires: expires_in_one_year});
		Cookies.set('login_hash', result.login_hash, {expires: expires_in_one_year});
		
		window.location.href = "/deck/deck_list.html";
		
		return;
	}

	$("#results-div").html(result.message);
}

function showCurrentUser()
{
	var username = Cookies.get('username');
	
	if (username)
	{
		$("#logged-in-container").show();
		$("#logged-in-as").show();
		$("#logged-in-as").html("Welcome, " + username);
		$("#log-out").show();
		$("#login-form").hide();
		$("#sign-in-button").hide();
		$("#create-account-container").hide();
		$("#view-decks-button").show();
	}
	else
	{
		$("#create-account-container").show();
		$("#login-form").show();
		$("#view-decks-button").hide();
		$("#sign-in-button").show();
	}
}

function doLogOut()
{
	$("#logged-in-as").hide();
	$("#log-out").hide();
	
	$("#login-form").show();
	$("#view-decks-button").hide();
	$("#sign-in-button").show();
	
	window.location.href = "/index.html";
	
	Cookies.remove('username');
	Cookies.remove('login_hash');
}